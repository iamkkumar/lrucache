package service;

import pojos.Node;

import java.util.HashMap;
import java.util.Map;

public class LRUCacheService {
    private Map<Integer, Node> list;
    private int capacity;
    Node head, tail;

    public LRUCacheService(int capacity){
        this.capacity=capacity;
        head=new Node(0,0);
        tail=new Node(0,0);
        list = new HashMap<>();
        head.next=tail;
        tail.prev=head;
    }

    public int getValue(int key){
        if(list.containsKey(key)){
            Node node=list.get(key);
            remove(node);
            insert(node);
            return node.value;
        }
        return -1;
    }

    public void setValue(int key, int value){
        if(list.containsKey(key)){
            Node node=list.get(key);
            node.value=value;
            remove(node);
            insert(node);
        }
        else{
            if(list.size()==capacity){
                remove(tail.prev);
            }
            Node node=new Node(key, value);
            insert(node);
        }
        System.out.print("call to setValue(), ");
    }

    void insert(Node node){
        list.put(node.key, node);
        Node headNext=head.next;
        head.next=node;
        node.prev=head;
        headNext.prev=node;
        node.next=headNext;
    }

    void remove(Node node){
        list.remove(node.key);
        node.prev.next=node.next;
        node.next.prev=node.prev;
    }
}
