import service.LRUCacheService;

public class LRUCacheApplication {

    public static void printVal(int value){
        System.out.print(value+", ");
    }

    public static void main(String[] args){
        LRUCacheService cache = new LRUCacheService(2);
        cache.setValue(1,1);
        cache.setValue(2,2);
        printVal(cache.getValue(1));
        cache.setValue(3,3);
        printVal(cache.getValue(2));
        cache.setValue(4,4);
        printVal(cache.getValue(1));
        printVal(cache.getValue(3));
        printVal(cache.getValue(4));
    }
}
